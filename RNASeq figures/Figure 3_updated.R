library(ggplot2)
library(ggpubr)
library(ape)
library(gridExtra)
library(plyr)
library(reshape)
library(xlsx)
library(edgeR)


############ all samples mds ############ 

raw_cnts = read.delim("featureCounts_matrix.txt",sep="\t",row.names = 1)
meta_data = read.delim("meta.txt")
rownames(meta_data) = meta_data$title


#meta_data = meta_data[meta_data$Option_2=="Birth",]
#meta_data = meta_data[rownames(meta_data) != "RNA_190950R",]

raw_cnts = raw_cnts[,rownames(meta_data)]
group = meta_data$BPD
rntu = rownames(raw_cnts)
raw_cnts = apply(raw_cnts,2,as.numeric)
rownames(raw_cnts) = rntu

x = raw_cnts
mnc = meta_data
group = factor(mnc$BPD)
sex = factor(mnc$Gender)


### EdgeR ###
y = DGEList(counts=x,group=group)
keep <- rowSums(cpm(y)>1) >= 6
y <- y[keep, , keep.lib.sizes=FALSE]
tokeep = rownames(y)
y = calcNormFactors(y)

datapc = plotMDS(y)
datapc.dist = pcoa(as.dist(datapc$distance.matrix))

PC_data.df = data.frame(PC1=datapc.dist$vectors[,1],PC2=datapc.dist$vectors[,2],meta_data)
PC_data.df$pf = paste(PC_data.df$BPD,PC_data.df$Option_2,sep="_")


mds_with_outlier = ggplot(PC_data.df,aes(x = PC1 , y = PC2,color=pf,shape=Option_2))+geom_point(size=5)+
  xlab(paste("PC1:",round(datapc.dist$values$Relative_eig[1]*100,2)," variation"))+
  ylab(paste("PC2:",round(datapc.dist$values$Relative_eig[2]*100,2)," variation"))+
  theme_classic()+
  stat_ellipse(aes(x = PC1,y = PC2, fill=pf),geom="polygon",level=0.7,alpha=0.3)+
  scale_fill_manual(values = c("#762A8D","#762A8D","#B0BF36","#B0BF36"))+
  scale_colour_manual(values = c("#762A8D","#762A8D","#B0BF36","#B0BF36"))+
  scale_shape_manual(values=c(19,25))




############ 36week_PMA samples only ############ 
raw_cnts = read.delim("featureCounts_matrix.txt",sep="\t",row.names = 1)
meta_data = read.delim("meta.txt")
rownames(meta_data) = meta_data$title
meta_data = meta_data[meta_data$Option_2=="36week_PMA",]
meta_data = meta_data[rownames(meta_data) != "RNA_190950",]
raw_cnts = raw_cnts[,rownames(meta_data)]
group = meta_data$BPD
rntu = rownames(raw_cnts)
raw_cnts = apply(raw_cnts,2,as.numeric)
rownames(raw_cnts) = rntu

x = raw_cnts
mnc = meta_data
mnc$BPD = factor(mnc$BPD,levels = c("No_BPD","BPD"))
group = factor(mnc$BPD)
sex = factor(mnc$Gender)

### EdgeR ###
y = DGEList(counts=x,group=group)
keep <- rowSums(cpm(y)>1) >= 10
y <- y[keep, , keep.lib.sizes=FALSE]
tokeep = rownames(y)


y = calcNormFactors(y)

datapc = plotMDS(y)
datapc.dist = pcoa(as.dist(datapc$distance.matrix))

PC_data.df = data.frame(PC1=datapc.dist$vectors[,1],PC2=datapc.dist$vectors[,2],meta_data)
PC_data.df$pf = paste(PC_data.df$BPD,PC_data.df$Option_2,sep="_")





design = model.matrix(~ sex + group)
normys = cpm(y)

y = estimateDisp(y, design, robust=TRUE)
fit = glmFit(y, design)
lrt = glmLRT(fit)

edgeR.res = data.frame(topTags(lrt,n=nrow(y)))
edgeR.res = edgeR.res[order(edgeR.res$logFC),]
edgeR.sig = edgeR.res[edgeR.res$FDR<0.05,]
Supp_table_4 = edgeR.res
Supp_table_4$fac = as.character(Supp_table_4$FDR < 0.05)
Supp_table_4$fac[Supp_table_4$FDR < 0.05 & Supp_table_4$logFC > 0] = "BPD"
Supp_table_4$fac[Supp_table_4$FDR < 0.05 & Supp_table_4$logFC < 0] = "No_BPD"

volcano_36 = ggplot(Supp_table_4,aes(x=logFC,y=-log10(FDR),color=fac))+geom_point()+geom_hline(yintercept = -log10(0.05))+
  scale_color_manual(values=c(c("#762A8D","black","#B0BF36")))

dim(edgeR.sig)




###### CTEN genes heatplot ######

meta_data = meta_data[meta_data$Option_2=="36week_PMA",] #Subset to timepoint
meta_data = meta_data[rownames(meta_data) != "RNA_190950",] # Remove outlier sample from consideration



## BiomaRt ##
library(biomaRt)
ensembl=useMart("ensembl")
ensembl = useDataset("hsapiens_gene_ensembl",mart=ensembl)
attributes = listAttributes(ensembl)
ensids=rownames(raw_cnts)
table(duplicated(ensids))
ens2gene = getBM(attributes=c('ensembl_gene_id','hgnc_symbol'), 
                 filters = 'ensembl_gene_id', 
                 values = ensids, 
                 mart = ensembl)
head(ens2gene)


ens2gene = ens2gene[!duplicated(ens2gene$ensembl_gene_id),]
gene2ens = ens2gene
gene2ens = gene2ens$hgnc_symbol


rownames(ens2gene) = ens2gene$ensembl_gene_id




###### CTEN genes heatplot ######
cten = read.csv("Human HECS database.csv")
upinBPD = c("CD71._EarlyErythroid","CD105._Endothelial")

cten = cten[cten$Tissue %in% c(upinBPD),]
cten$Tissue = factor(cten$Tissue,levels=c(upinBPD))
cten = cten[order(cten$Tissue),]
head(cten)
normys_cten = normys
rownames(normys_cten) = ens2gene[rownames(normys_cten),"hgnc_symbol"]
cten = cten[as.character(cten$Symbol) %in% rownames(normys_cten),]
normys_cten = normys_cten[as.character(cten$Symbol),]
meta_data36 = meta_data[order(meta_data$BPD),]
normys_cten2 = normys_cten[as.character(cten[cten$Tissue=="CD71._EarlyErythroid","Symbol"]),]




library(ComplexHeatmap)
ha = HeatmapAnnotation(df = meta_data36[,c("BPD","BPD2","Steroids","Birth_mode","Gender","Treatment")],
                       birth_weight = anno_points(meta_data$birth_weight),
                       Gestational_age = anno_points(meta_data$GA_weeks,ylim(range(meta_data$GA_weeks+10))),
                       col = list(BPD = c("BPD" =  "#762A8D", "No_BPD" = "#B0BF36"),
                                  BPD2 = c("Severe" =  "darkorchid4", "Moderate" = "lightpink2","Mild" = "greenyellow","None" = "forestgreen"),
                                  Steroids = c("Steroids" =  "black", "No_steroids" = "grey"),
                                  Birth_mode = c("Vaginal" = "lightgreen","Caesarean_section" = "darkgoldenrod2"),
                                  Gender = c("Male" = "deepskyblue","Female" = "pink"),
                                  Treatment = c("DHA" = "chocolate3","Control" = "springgreen3")
                       ))


zzz <- t(scale(t(normys_cten2[,rownames(meta_data36)]))) #Scale to Z scores by Row. 
Heatmap(zzz,show_row_names = FALSE,top_annotation = ha,col=c("blue3","dodgerblue1","white","firebrick3","red2","red3"),
        column_title ="CD71+ Early Erythroid associated genes")

